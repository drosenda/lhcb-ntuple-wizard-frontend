/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
const analysisHelpers = [
    {
        name: "__init__.py",
        content: ``,
    },
    {
        name: "decaytreetuple.py",
        content: `from Configurables import DaVinci, DecayTreeTuple, MCDecayTreeTuple
from DecayTreeTuple.Configuration import addTupleTool

try:
    from six import string_types
except ImportError:
    string_types = (str, unicode)

def parse_tools(configurable, tools, verbose = False):
    configurable_name = configurable.name()
    for tool in tools:
        if isinstance(tool, string_types):
            configurable.ToolList += [tool]
            if verbose: print("Adding {tool} to {configurable_name}.ToolList".format(**locals()))
        elif isinstance(tool, dict):
            assert(len(tool) == 1) # must have one key: the name
            name = [key for key in tool][0]
            if verbose: print("Adding {name} to {configurable_name}".format(**locals()))
            tool_conf = configurable.addTupleTool(name)
            tool_conf_name = tool_conf.name()
            for option, value in tool[name].items():
                if verbose: print("\t{tool_conf_name}.{option} = {value}".format(**locals()))
                tool_conf.setProp(option, value)

def configure_dtt(config, verbose = False):
    dtt_class, dtt_name = config["name"].split("/")
    if dtt_class == "DecayTreeTuple":
        dtt = DecayTreeTuple(dtt_name)
    elif dtt_class == "MCDecayTreeTuple":
        dtt = MCDecayTreeTuple(dtt_name)
    else:
        raise ValueError("Class "+dtt_class+" not recognised")
    dtt.setDescriptorTemplate(config["descriptorTemplate"])
    dtt.Inputs = config["inputs"]
    if DaVinci().InputType == "MDST":
        dtt.Inputs = [i.removeprefix(DaVinci().RootInTES+"/") for i in dtt.Inputs]
    dtt.ToolList = [] # Usually constructed with a default ToolList, but we want to replace that
    if "tools" in config:
        parse_tools(dtt, config["tools"], verbose)
    if "branches" in config:
        for particle in config["branches"]:
            if "tools" in config["branches"][particle]:
                parse_tools(getattr(dtt, particle), config["branches"][particle]["tools"], verbose)
    if "groups" in config:
        for group in config["groups"]:
            for particle in group.split(","):
                if "tools" in config["groups"][group]:
                    parse_tools(getattr(dtt, particle), config["groups"][group]["tools"], verbose)
    if verbose:
        print(dtt)
        for particle in config["branches"]:
            print(getattr(dtt, particle))
    return dtt

def save_config(dtt, verbose = False):
    """
        Need to capture all non-blank properties and those of all child configurables
    """
    raise NotImplementedError()

`,
    },
    {
        name: "dst_writer.py",
        content: `from DSTWriters.Configuration import SelDSTWriter, stripDSTStreamConf, stripDSTElements, stripMicroDSTStreamConf, stripMicroDSTElements

def write_dst(selection_sequences, enable_packing = True, selective_raw_event = False, microDST = False, is_mc = None, extra_items = None):
    # Configuration of SelDSTWriter
    if microDST:
        elements = {"default": stripMicroDSTElements(pack = enable_packing, isMC = is_mc)}
        config = {"default": stripMicroDSTStreamConf(pack = enable_packing, isMC = is_mc)}
    else:
        elements = {"default": stripDSTElements(pack = enable_packing)}
        config = {"default": stripDSTStreamConf(pack = enable_packing, selectiveRawEvent = selective_raw_event)}
    if extra_items:
        config["default"].extraItems += extra_items
    dst_writer = SelDSTWriter("MyDSTWriter", StreamConf = config, MicroDSTElements = elements, OutputFileSuffix = "0"*6,  SelectionSequences = selection_sequences)
    return dst_writer.sequence()

`,
    },
    {
        name: "stripping.py",
        content: `from Configurables import EventNodeKiller, GaudiSequencer, ProcStatusCheck, StrippingTCK
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive

import re

def line_name_is(name):
    """
    Match stripping lines by exact name
    """
    return lambda l: l.name() == name

def line_name_contains(phrase):
    """
    Match stripping lines whose name contains a specific string
    """
    return lambda l: phrase in l.name()

def line_name_matches(expression):
    """
    Match stripping lines whose name matches a regular expression
    """
    p = re.compile(expression)
    return lambda l: p.match(l.name())

def line_name_in_list(name_list):
    """
    Match stripping lines whose name appears in a list
    """
    return lambda l: l.name() in name_list

def stripping_tck(version, tes_prefix = "Strip"):
    """
    Form a stripping TCK from the stripping version.
    The pattern is VVVVSSSS where V are digits from the DaVinci version, and S are digits from the Stripping version.
    The DaVinci version is taken from the MC production step. NB: this may not match the version used to strip data, due to MC-specific patches.
    The major version numbers have 2 digits of space, so they are written like the decimal number.
    The p and r version numbers have only 1 digit, so numbers 10 to 15 are represented by letters A-F.
    """
    tck = {
        "21r1"  : 0x36152110,
        "21r1p1": 0x39112111,
        "21r1p2": 0x39162112,
        "21"    : 0x36152100,
        "21r0p1": 0x39112101,
        "21r0p2": 0x39162102,
        "24r2"  : 0x44A52420,
        "28r2"  : 0x44A52820,
        "29r2"  : 0x42732920,
        "29r2p1": 0x42922921,
        "34"    : 0x44703400,
        "34r0p1": 0x44A23401,
    }[version]
    return StrippingTCK(HDRLocation = "/Event/{}/Phys/DecReports".format(tes_prefix), TCK = tck)

stripping_dv_versions = {
    "21r1"  : "v36r1p5",
    "21r1p1": "v39r1p1",
    "21r1p2": "v39r1p6",
    "21"    : "v36r1p5",
    "21r0p1": "v39r1p1",
    "21r0p2": "v39r1p6",
    "24r2"  : "v44r10p5",
    "28r2"  : "v44r10p5",
    "29r2"  : "v42r7p3",
    "29r2p1": "v42r9p2",
    "34"    : "v44r7",
    "34r0p1": "v44r10p2",
}

event_node_killer = EventNodeKiller("StripKiller", Nodes = ["/Event/AllStreams", "/Event/Strip"])

def custom_stripping_stream(version, line_filter, stream_name):
    """
    Build a custom stripping stream with the desired lines
    """
    # Build streams
    stripping_name = "stripping{}".format(version)
    conf = strippingConfiguration(stripping_name)
    archive = strippingArchive(stripping_name)
    all_streams = buildStreams(conf, archive)
    custom_stream = StrippingStream(stream_name)
    # Extract the desired lines and add them to the new stream
    for stream in all_streams:
        matched_lines = filter(line_filter, stream.lines)
        custom_stream.appendLines(matched_lines)
    return custom_stream

def stripping_mc_filter(version, line_filter, stream_name = "Filter"):
    """
    Build custom stripping sequence with standard options for filtered MC production.
    """
    # Configure the stripping
    custom_stream = custom_stripping_stream(version, line_filter, stream_name)
    stripping_conf = StrippingConf(Streams = [custom_stream], MaxCandidates = 2000, TESPrefix = "Strip", AcceptBadEvents = False, BadEventSelection = ProcStatusCheck())
    custom_stream.sequence().IgnoreFilterPassed = False
    # Combine the sequences
    seq = GaudiSequencer("{}Sequence".format(stream_name))
    seq.Members += [stripping_conf.sequence(), stripping_tck(version, tes_prefix)]

def restripping(version, line_filter = line_name_contains("Stripping"), stream_name = "AllStreams", tes_prefix = "Strip", max_candidates = 2000, accept_bad_events = False, ignore_filter_passed = False):
    """
    Build custom stripping sequence for running on an already-stripped sample.
    """
    # Configure the stripping
    custom_stream = custom_stripping_stream(version, line_filter, stream_name)
    stripping_conf = StrippingConf(Streams = [custom_stream], MaxCandidates = max_candidates, TESPrefix = tes_prefix, AcceptBadEvents = accept_bad_events, BadEventSelection = ProcStatusCheck())
    custom_stream.sequence().IgnoreFilterPassed = ignore_filter_passed
    # Combine the sequences
    seq = GaudiSequencer("{}Sequence".format(stream_name))
    seq.Members += [event_node_killer, stripping_conf.sequence()]
    return seq

`,
    },
];

export default analysisHelpers;
