/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import _ from "lodash";

class DTTConfig {
    /* Class for customising the configuration of a DecayTreeTuple algorithm.
     * The output will be parsed by a custom parser in the AnalysisHelpers data-package.
     */
    constructor(config, toolMetadata) {
        this.metadata = toolMetadata;
        this.config = config;
    }

    static createDTT(descriptorTemplate, branchMap, inputs, name, toolMetadata) {
        let branchDict = {};
        branchMap.forEach((bmap) => {
            const branch = bmap.branch;
            const particle = bmap.particle;
            branchDict[branch] = {
                particle: particle,
                tools: [],
            };
        });
        /* Dict to store the DecayTreeTuple options.
         * This is what should be written to the output JSON file, with a bit of cleaning up.
         */
        const config = {
            inputs: inputs,
            descriptorTemplate: descriptorTemplate,
            tools: [],
            branches: branchDict,
            groups: {},
        };
        let dtt = new DTTConfig(config, toolMetadata);
        const defaultTools = [
            // default tool list in DaVinci v45r7
            // TODO: load this from a file generated on deployment
            "TupleToolKinematic",
            "TupleToolPid",
            "TupleToolANNPID",
            "TupleToolGeometry",
            "TupleToolEventInfo",
        ];
        defaultTools.forEach((tool) => {
            dtt.addTool("", tool);
        });
        dtt.setName(name);
        return dtt;
    }

    // Set the input location (TES location)
    setInputs(inputs) {
        this.config.inputs = inputs;
    }
    // Set the class+name combination
    setName(name) {
        const safeName = name.replace(/\W/g, "");
        this.config.name = `DecayTreeTuple/${safeName}`;
    }

    // Return the name of the DecayTreeTuple algorithm
    getName() {
        return this.config.name.split("/")[1];
    }

    // Return the name of the DecayTreeTuple algorithm, or default value "" if empty
    getSafeName() {
        const name = this.getName();
        return name ? name : "";
    }

    // Delete all groups with an empty tool list
    pruneGroups() {
        const empty = Object.keys(this.config.groups).filter((group) => this.config.groups[group].tools.length === 0);
        empty.forEach((key) => delete this.config.groups[key]);
    }

    // Find the appropriate object which has a "tools" list
    getTarget(branch) {
        this.pruneGroups();
        if (branch.length === 0) {
            return this.config;
        } else if (`${branch}` in this.config.branches) {
            return this.config.branches[branch];
        } else {
            if (!(`${branch}` in this.config.groups)) {
                this.config.groups[`${branch}`] = {
                    particles: branch.map((element) => this.config.branches[element].particle),
                    tools: [],
                };
            }
            return this.config.groups[`${branch}`];
        }
    }

    // Return the list index of a tool
    findTool(target, toolString) {
        const index = target.tools.findIndex((tool) => toolString in tool);
        if (index < 0) {
            throw new Error(`${toolString} not found in ${target}`);
        }
        return index;
    }

    // Add a TupleTool. Return boolean denoting success
    addTool(branch, toolClass, toolName = "") {
        let target = this.getTarget(branch);
        // The toolString identifies the tool in the target's tool list
        // If the tool is given a name (recommended), append it following a slash
        const toolString = toolName.length ? `${toolClass}/${toolName}` : toolClass;
        // Add the tool to the list if not already there
        try {
            this.findTool(target, toolString);
        } catch (e) {
            const toolConfig = {
                [toolString]: this.getDefaultConfig(toolClass),
            };
            target.tools.push(toolConfig);
            return true;
        }
        throw new Error(`${toolString} already exists in ${target}`);
    }

    // Return a dict containing the default configuration of a TupleTool
    getDefaultConfig(toolClass) {
        if (toolClass in this.metadata) {
            let params = {};
            this.metadata[toolClass].interface.forEach((parameter) => {
                params[parameter.name] = _.cloneDeep(parameter["default"]); // Deep copy
            });
            return params;
        } else {
            return {};
        }
    }

    // Configure a TupleTool by passing a dict (ideally a modified version of the one returned by getToolConfig)
    configureTool(branch, toolString, parameters) {
        let target = this.getTarget(branch);
        const index = this.findTool(target, toolString);
        target.tools[index][toolString] = parameters;
    }

    // Return the current config dict of a tool, so it can be modified by the user
    getToolConfig(branch, toolString) {
        const target = this.getTarget(branch);
        const index = this.findTool(target, toolString);
        return target.tools[index][toolString];
    }

    // Delete a tool
    removeTool(branch, toolString) {
        let target = this.getTarget(branch);
        const index = this.findTool(target, toolString);
        target.tools.splice(index, 1);
    }

    // Return a list of strings corresponding to the target's tool list
    listTools(branch) {
        const target = this.getTarget(branch);
        return target.tools.map((tool) => Object.keys(tool)[0]).sort();
    }
}

export default DTTConfig;
