/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

import {mathjax} from "mathjax-full/js/mathjax";
import {TeX} from "mathjax-full/js/input/tex";
import {SVG} from "mathjax-full/js/output/svg";

export function tex2svg(inputTeX) {
    const tex = new TeX({packages: ["base", "ams"]});
    const svg = new SVG({fontCache: "none"});
    const tex_html = mathjax.document("", {
        InputJax: tex,
        OutputJax: svg,
    });
    return tex_html.convert(inputTeX);
}

export default tex2svg;
