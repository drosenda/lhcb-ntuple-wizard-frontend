/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
class BKPath {
    constructor(path) {
        this.path = path;
        this.isMC = path.split("/")[1] === "MC";
    }

    getStrippingVersion() {
        return this.path.split("/")[this.isMC ? 7 : 6].replace("Stripping", "").replace("NoPrescalingFlagged", "");
    }

    getYear() {
        return this.path.split("/")[2].replace("Collision", "20");
    }

    getPolarity() {
        const matchingPolarities = ["MagUp", "MagDown"].filter((polarity) => this.path.includes(polarity));
        if (matchingPolarities.length === 1) {
            return matchingPolarities;
        } else {
            return "?";
        }
    }

    getFilename() {
        return this.path.split("/")[this.isMC ? 9 : 8];
    }

    getEventType() {
        return this.path.split("/")[this.isMC ? 8 : 7];
    }

    isValid() {
        const bkArray = this.path.split("/");
        if (bkArray[0] !== "") {
            return false;
        }
        if (!this.path.toLowerCase().endsWith("dst")) {
            return false;
        }
        if (bkArray.length === 9 && bkArray[1] === "LHCb") {
            return true;
        } else if (bkArray.length === 10 && bkArray[1] === "MC") {
            return true;
        }
        return false;
    }
}

export default BKPath;
