/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import {MathComponent} from "mathjax-react";
import pako from "pako";
import config from "../config";

import JsZip from "jszip";

export async function loadDict(path) {
    const url = config["metadata_baseurl"] + path + ".json.gz";
    const response = await fetch(url);
    if (!response.ok) {
        console.error(response);
        return null;
    }
    const data = await response.arrayBuffer();
    const unzipped = await pako.inflate(new Uint8Array(data));
    const decoded = new TextDecoder("utf-8").decode(unzipped);
    const content = JSON.parse(decoded);
    return content;
}

export function dec_to_options(decays) {
    const arr = decays.map((decay) =>
        Object({
            value: decay.list.plain,
            label: <MathComponent tex={decay.descriptors.latex} display={false} />,
        })
    );
    return arr;
}

export function tupletools_to_option(toolDict) {
    let arr = [];

    Object.keys(toolDict).forEach((tool) => {
        arr.push({value: toolDict[tool]["description"], label: tool});
    });
    return arr;
}
export function list_to_options(lines) {
    let arr = [];
    lines.forEach((line) => {
        arr.push({value: line, label: line});
    });
    return arr;
}

export function parse_searchitem_descriptions(data) {
    let arr = [];
    Object.keys(data).forEach((name) => {
        arr.push({[name]: data[name]["description"]});
    });
    return arr;
}

export function parse_tupleTools(data) {
    let tupleTools = {};
    Object.keys(data).forEach((item) => {
        tupleTools[item] = data[item]["description"];
    });
    return tupleTools;
}

export function get_tupleTool_description(tool_name, data) {
    let key_arr = Object.keys(data).filter((item) => item === tool_name);
    if (key_arr.length > 0) {
        return data[key_arr[0]]["description"];
    }
    return "";
}
export function get_tupleTool_config(tool_name, data) {
    let key_arr = Object.keys(data).filter((item) => item === tool_name);
    if (key_arr.length > 0) {
        return data[key_arr[0]]["description"];
    }
    return "";
}

export function downloadConfig_old(output, name = "file.txt") {
    const blob = new Blob([output], {type: "text/plain"});
    //if (
    //  window.navigator &&
    //  window.navigator.msSaveOrOpenBlob
    //) return window.navigator.msSaveOrOpenBlob(blob);

    // For other browsers:
    // Create a link pointing to the ObjectURL containing the blob.
    const data = window.URL.createObjectURL(blob);

    const link = document.createElement("a");
    link.href = data;
    link.download = name;

    // this is necessary as link.click() does not work on the latest firefox
    link.dispatchEvent(
        new MouseEvent("click", {
            bubbles: true,
            cancelable: true,
            view: window,
        })
    );

    setTimeout(() => {
        // For Firefox it is necessary to delay revoking the ObjectURL
        window.URL.revokeObjectURL(data);
        link.remove();
    }, 100);
}

export function downloadBlob(blob, name) {
    // Convert your blob into a Blob URL (a special url that points to an object in the browser's memory)
    const blobUrl = URL.createObjectURL(blob);

    // Create a link element
    const link = document.createElement("a");

    // Set link's href to point to the Blob URL
    link.href = blobUrl;
    link.download = name;

    // Append link to the body
    document.body.appendChild(link);

    // Dispatch click event on the link
    // This is necessary as link.click() does not work on the latest firefox
    link.dispatchEvent(
        new MouseEvent("click", {
            bubbles: true,
            cancelable: true,
            view: window,
        })
    );
    // Remove link from body
    document.body.removeChild(link);
}

export function download(output, name = "file.txt", type = "text/plain") {
    const blob = new Blob([output], {type: type});
    downloadBlob(blob, name);
}

export function downloadZip(files, name = "archive.zip") {
    const zip = JsZip();
    files.forEach(([name, output]) => {
        const blob = new Blob([output], {type: "text/plain"});
        zip.file(name, blob);
    });
    zip.generateAsync({type: "blob"}).then((blob) => downloadBlob(blob, name));
}

export function getDTreeName(mappedList) {
    let name = "";
    mappedList.forEach((part) => {
        name += part["particle"];
    });
    //let name = descriptor.split(' ').join('');
    return name;
}
