/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import React from "react";
import PropTypes from "prop-types";
import {Badge, OverlayTrigger, Tooltip, Spinner} from "react-bootstrap";
import MetadataContext from "../contexts/MetadataContext";

class DecayTag extends React.Component {
    static contextType = MetadataContext;

    render() {
        const background =
            this.context.loaded.userHints && this.context.metadata.userHints.decayTags[this.props.tag].warn
                ? "danger"
                : "warning";
        const foreground =
            this.context.loaded.userHints && this.context.metadata.userHints.decayTags[this.props.tag].warn
                ? "light"
                : "dark";
        const description = this.context.loaded.userHints ? (
            this.context.metadata.userHints.decayTags[this.props.tag].description
        ) : (
            <Spinner animation="border" />
        );
        return (
            <OverlayTrigger overlay={<Tooltip>{description}</Tooltip>}>
                <Badge pill bg={background} text={foreground} {...this.props}>
                    {this.props.tag}
                </Badge>
            </OverlayTrigger>
        );
    }
}

DecayTag.propTypes = {
    tag: PropTypes.string,
};
export default DecayTag;
