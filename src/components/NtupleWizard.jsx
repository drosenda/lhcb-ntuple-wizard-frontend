/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import "bootstrap/dist/css/bootstrap.css";
import _ from "lodash";
import PropTypes from "prop-types";
import React from "react";
import {Route, BrowserRouter as Router, Switch} from "react-router-dom";
import {MetadataProvider} from "../contexts/MetadataContext";
import DescriptorsSearch from "./DescriptorsSearch";
import LinesTable from "./LinesTable";
import SelectVariables from "./SelectVariables";

class NtupleWizard extends React.Component {
    state = {
        rows: JSON.parse(localStorage.getItem("rows") || "[]"),
        productionName: localStorage.getItem("name") || "",
        contactEmail: (localStorage.getItem("email") || "").split(/,/).filter((s) => s),
        reasonForRequest: localStorage.getItem("reasonForRequest") || "",
    };

    callbackUpdateRows = (rows) => {
        const newRows = rows.map((row) => ({
            ...row,
            dtt: row.dtt ? _.cloneDeep(row.dtt.config) : false,
        }));
        this.setState({
            rows: newRows,
        });
        const rowString = JSON.stringify(newRows);
        localStorage.setItem("rows", rowString);
    };

    callbackUpdateInfo = (name, email, reasonForRequest) => {
        this.setState({
            productionName: name,
            contactEmail: email,
            reasonForRequest: reasonForRequest,
        });
        localStorage.setItem("name", name);
        localStorage.setItem("email", email.join(","));
        localStorage.setItem("reasonForRequest", reasonForRequest);
    };

    render() {
        let {
            basePath,
            decaysPath,
            variablesPath,
            contactEmail,
            submitLocation,
            hideDownloadButtons,
            requestReasonMessage,
            requestSubmittedMessage,
        } = this.props;

        let emailIsKnown = false;
        if (contactEmail) {
            localStorage.setItem("email", contactEmail);
            emailIsKnown = true;
        }

        if (!submitLocation) {
            hideDownloadButtons = false;
        }

        return (
            <MetadataProvider>
                <Router>
                    <Switch>
                        <Route exact path={basePath}>
                            <LinesTable
                                parentCallbackRows={this.callbackUpdateRows}
                                parentCallbackInfo={this.callbackUpdateInfo}
                                rows={this.state.rows}
                                name={this.state.productionName}
                                email={this.state.contactEmail}
                                reasonForRequest={this.state.reasonForRequest}
                                decaysPath={decaysPath}
                                variablesPath={variablesPath}
                                submitLocation={submitLocation}
                                hideDownloadButtons={hideDownloadButtons}
                                emailIsKnown={emailIsKnown}
                                requestReasonMessage={requestReasonMessage}
                                requestSubmittedMessage={requestSubmittedMessage}
                            />
                        </Route>
                        <Route exact path={decaysPath}>
                            <DescriptorsSearch
                                parentCallback={this.callbackUpdateRows}
                                rows={this.state.rows}
                                basePath={basePath}
                            />
                        </Route>
                        <Route exact path={variablesPath}>
                            <SelectVariables
                                parentCallback={this.callbackUpdateRows}
                                rows={this.state.rows}
                                basePath={basePath}
                            />
                        </Route>
                    </Switch>
                </Router>
            </MetadataProvider>
        );
    }
}

NtupleWizard.propTypes = {
    basePath: PropTypes.string.isRequired,
    decaysPath: PropTypes.string.isRequired,
    variablesPath: PropTypes.string.isRequired,
    contactEmail: PropTypes.string,
    submitLocation: PropTypes.string,
    hideDownloadButtons: PropTypes.bool,
    requestReasonMessage: PropTypes.string,
    requestSubmittedMessage: PropTypes.object,
};

export default NtupleWizard;
