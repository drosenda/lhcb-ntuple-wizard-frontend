/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import React from "react";
import PropTypes from "prop-types";
import {Badge} from "react-bootstrap";

class EventTypeBadge extends React.Component {
    render() {
        const content = "" + this.props.eventType === "90000000" ? "Data" : "" + this.props.eventType;
        const linkProps =
            content === "Data"
                ? {}
                : {
                      as: "a",
                      href: "http://lhcbdoc.web.cern.ch/lhcbdoc/decfiles/",
                      target: "_blank",
                  };
        return (
            <Badge pill {...linkProps} bg="primary">
                {content}
            </Badge>
        );
    }
}

EventTypeBadge.propTypes = {
    eventType: PropTypes.string,
};
export default EventTypeBadge;
