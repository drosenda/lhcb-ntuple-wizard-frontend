/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import "bootstrap/dist/css/bootstrap.css";
import React from "react";
import {Container, Nav, Navbar, NavDropdown} from "react-bootstrap";
import {Archive, CodeSlash, House} from "react-bootstrap-icons";
import config from "../config";
import NtupleWizard from "./NtupleWizard";

class App extends React.Component {
    render() {
        return (
            <>
                <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
                    <Container>
                        <Navbar.Brand href="/">
                            <img src="/logo.svg" height="30" className="d-inline-block align-top" alt="LHCb logo" />{" "}
                            LHCb NTuple Wizard
                        </Navbar.Brand>
                        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                        <Navbar.Collapse className="justify-content-end">
                            <Nav>
                                <NavDropdown title="About" id="about-dropdown" menuVariant="dark">
                                    <NavDropdown.Item
                                        href="https://lhcb-dpa.web.cern.ch/lhcb-dpa/wp6/ntupling-wizard.html"
                                        target="_blank"
                                    >
                                        <House /> Project home
                                    </NavDropdown.Item>
                                    <NavDropdown.Item
                                        href="https://gitlab.cern.ch/lhcb-dpa/wp6-analysis-preservation-and-open-data/lhcb-ntuple-wizard-frontend"
                                        target="_blank"
                                    >
                                        <CodeSlash /> Source code
                                    </NavDropdown.Item>
                                    <NavDropdown.Item href={config.metadata_baseurl} target="_blank">
                                        <Archive /> Metadata files
                                    </NavDropdown.Item>
                                </NavDropdown>
                                <Nav.Link href="https://opendata.cern.ch/" target="_blank">
                                    <img src="/open_data_portal.png" height="30" alt="CERN Open Data Portal" />
                                </Nav.Link>
                            </Nav>
                        </Navbar.Collapse>
                    </Container>
                </Navbar>
                <Container className="mt-4">
                    <NtupleWizard
                        basePath="/"
                        decaysPath="/decays"
                        variablesPath="/variables"
                        submitLocation="" // Leave empty to hide the submit button -> overrides hideDownloadButtons to false
                        contactEmail="" // Leave empty to let the user fill in their email address
                        hideDownloadButtons={false} // Can be overridden by submitLocation
                        requestReasonMessage="Please provide a reason for your request."
                        requestSubmittedMessage={<>Your request has been submitted.</>}
                    />
                </Container>
            </>
        );
    }
}

export default App;
