/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import EmailValidator from "email-validator";
import yaml from "js-yaml";
import PropTypes from "prop-types";
import React from "react";
import {
    Alert,
    Badge,
    Button,
    ButtonGroup,
    Card,
    Col,
    Form,
    FormControl,
    InputGroup,
    Modal,
    OverlayTrigger,
    Popover,
    Row,
    Spinner,
    Tooltip,
} from "react-bootstrap";
import {Download, FileEarmarkCode, PencilSquare, PlusLg, QuestionCircle, Send, Upload} from "react-bootstrap-icons";
import {Route} from "react-router-dom";
import Select from "react-select";
import Creatable from "react-select/creatable";
import config from "../config";
import MetadataContext from "../contexts/MetadataContext";
import BKPath from "../lib/BKPath";
import DTTConfig from "../lib/DTTConfig";
import analysisHelpers from "../lib/analysisHelpers";
import {download, downloadZip} from "../lib/utils";
import Dataset from "./Dataset";
import Decay from "./Decay";
import DeleteButton from "./DeleteButton";
import StrippingLine from "./StrippingLine";

function streamLinesToOption(row) {
    let opts = {};
    let streamArr = [];
    let lineArr = [];
    let groups = [];
    Object.keys(row.decay.lines).forEach((streamLine) => {
        const [stream, line] = streamLine.split("/");
        streamArr.push(stream);
        lineArr.push(line);
    });
    const uniqStreamArr = [...new Set(streamArr)];
    uniqStreamArr.forEach((uniq) => {
        opts[uniq] = [];
        lineArr.forEach((element, index) => {
            if (streamArr[index] === uniq) {
                let dup = false;
                const opt = {
                    value: element,
                    label: (
                        <StrippingLine line={element} stream={uniq} versions={row.decay.lines[uniq + "/" + element]} />
                    ),
                    group: uniq,
                };
                opts[uniq].forEach((obj) => {
                    if (obj["label"] === element) {
                        dup = true;
                    }
                });
                if (!dup) {
                    opts[uniq].push(opt);
                }
            }
        });
        groups.push({label: uniq, options: opts[uniq]});
    });
    return groups;
}

function pathToOption(path) {
    return Object({
        value: path,
        label: <Dataset path={path} />,
    });
}

class LinesTable extends React.Component {
    constructor(props) {
        super(props);
        this.propsVar = this.props;
    }

    static contextType = MetadataContext;
    state = {
        rows: this.props.rows.map((row) => ({
            ...row,
            dtt: row.dtt ? new DTTConfig(row.dtt, []) : false,
        })),
        showUploadModal: false,
        modalID: -99,
        productionName: this.props.name,
        contactEmail: this.props.email,
        reasonForRequest: this.props.reasonForRequest,
        submitLocation: this.props.submitLocation,
        hideDownloadButtons: this.props.hideDownloadButtons,
        requestLoading: false,
        requestSubmitted: false,
        emailIsKnown: this.props.emailIsKnown,
        showReasonForRequestModal: false,
        requestReasonMessage: this.props.requestReasonMessage,
        requestSubmittedMessage: this.props.requestSubmittedMessage,
    };

    handleSubmitRows = (history) => {
        let rows = [...this.state.rows];
        rows.forEach((row) => {
            row.editTree = true;
        });
        this.props.parentCallbackRows(rows);
        this.props.parentCallbackInfo(this.state.productionName, this.state.contactEmail, this.state.reasonForRequest);
        history.push(this.propsVar.variablesPath);
    };

    handleSubmitSpecificRow = (history, idx) => {
        let rows = [...this.state.rows];
        rows.forEach((row) => {
            row.editTree = row.id === idx;
        });
        this.props.parentCallbackRows(rows);
        this.props.parentCallbackInfo(this.state.productionName, this.state.contactEmail, this.state.reasonForRequest);
        history.push(this.propsVar.variablesPath);
    };

    handleAddRow = (history) => {
        this.props.parentCallbackInfo(this.state.productionName, this.state.contactEmail, this.state.reasonForRequest);
        history.push(this.propsVar.decaysPath);
    };

    handleRemoveSpecificRow = (id) => () => {
        let rows = [...this.state.rows];
        rows = rows.filter((row) => row.id !== id);
        this.setState({rows});
        this.props.parentCallbackRows(rows);
    };

    handleRemoveSpecificRowDTT = (id) => () => {
        let rows = [...this.state.rows];
        const index = this.state.rows.findIndex((item) => item.id === id);
        rows[index].dtt = false;
        this.setState({rows});
        this.props.parentCallbackRows(rows);
    };

    handleCreateDTT = (id) => () => {
        let rows = [...this.state.rows];
        const index = this.state.rows.findIndex((item) => item.id === id);
        rows[index].dtt = DTTConfig.createDTT(
            rows[index].decay.descriptors.template,
            rows[index].decay.descriptors.mapped_list.flat(Infinity),
            [],
            "",
            this.context.metadata.tupleTools.tupleTools
        );
        this.setInputs(id, rows[index].lines);
        this.setState({rows});
        this.props.parentCallbackRows(rows);
    };

    setInputs = (id, lines) => {
        let rows = [...this.state.rows];
        const index = this.state.rows.findIndex((item) => item.id === id);
        const inputs = lines.map(({stream, line}) => {
            const lineLocation = String(line).replace("Stripping", "");
            return `/Event/${stream}/Phys/${lineLocation}/Particles`;
        });
        rows[index].dtt.setInputs(inputs);
        this.setState({rows});
        this.props.parentCallbackRows(rows);
    };

    changeSelStrLines = (id) => (choices) => {
        let rows = [...this.state.rows];
        const index = this.state.rows.findIndex((item) => item.id === id); // All choices have the same value (row.id)

        rows[index].lines = choices.map((choice) =>
            Object({
                stream: choice.group,
                line: choice.value,
                versions: this.state.rows[index].decay.lines[choice.group + "/" + choice.value],
            })
        );
        this.setState({rows});
        this.pathChoiceHandler(id)(rows[index].paths.map((path) => ({value: path})));
        if (rows[index].dtt) {
            this.setInputs(id, rows[index].lines);
        }
    };

    validatePath = (choice, lines) => {
        const path = new BKPath(choice);
        if (!path.isValid()) {
            return false;
        }
        return lines.every((line) => {
            const streamName = line.stream.toLowerCase();
            const filename = path.getFilename().split(".")[0].toLowerCase();
            const matchingVersion = line.versions.some((version) => path.getStrippingVersion() === version);
            return ["allstreams", streamName].includes(filename) && matchingVersion;
        });
    };

    pathChoiceHandler = (id) => (choices) => {
        let rows = [...this.state.rows];
        const index = this.state.rows.findIndex((item) => item.id === id); // All choices have the same value (row.id)
        const validPaths = choices
            .filter((choice) => this.validatePath(choice.value, this.state.rows[index].lines))
            .map((choice) => choice.value);
        const validPathOptions = rows[index].pathOptions.filter((choice) =>
            this.validatePath(choice, this.state.rows[index].lines)
        );
        rows[index].paths = validPaths;
        rows[index].pathOptions = validPathOptions;
        this.setState({rows});
        this.props.parentCallbackRows(rows);
    };

    pathCreateHandler = (id) => (choice) => {
        let rows = [...this.state.rows];
        const index = this.state.rows.findIndex((item) => item.id === id); // All choices have the same value (row.id)
        if (this.validatePath(choice.value, this.state.rows[index].lines)) {
            rows[index].paths.push(choice.value);
            rows[index].pathOptions.push(choice.value);
        }
        this.setState({rows});
    };

    setRowName =
        (id) =>
        ({target}) => {
            let rows = [...this.state.rows];
            const index = this.state.rows.findIndex((item) => item.id === id);
            rows[index].dtt.setName(target.value);
            this.setState({rows});
            this.props.parentCallbackRows(rows);
        };

    setProductionName = ({target}) => {
        const safeName = target.value.replaceAll(/[^\w]/g, "");
        this.setState({productionName: safeName});
        this.props.parentCallbackInfo(safeName, this.state.contactEmail, this.state.reasonForRequest);
    };

    setContactEmail = ({target}) => {
        const emails = target.value.split(/[\s,]+/);
        this.setState({contactEmail: emails});
        const validEmails = emails.filter((email) => EmailValidator.validate(email));
        this.props.parentCallbackInfo(this.state.productionName, validEmails, this.state.reasonForRequest);
    };

    setReasonForRequest = ({target}) => {
        const reasonForRequest = target.value;
        this.setState({reasonForRequest: reasonForRequest});
        this.props.parentCallbackInfo(this.state.productionName, this.state.contactEmail, reasonForRequest);
    };

    clearAll = () => {
        this.setState({productionName: "", contactEmail: [], rows: [], reasonForRequest: ""});
        this.props.parentCallbackRows([]);
        this.props.parentCallbackInfo("", [], "");
    };

    getBKPaths = (id) => {
        const index = this.state.rows.findIndex((item) => item.id === id);
        const row = this.state.rows[index];
        const options = row.paths.map((path) => pathToOption(path));
        return options;
    };

    defaultBKPaths = (id) => {
        const index = this.state.rows.findIndex((item) => item.id === id);
        const row = this.state.rows[index];
        const defaultOptions = this.context.loaded.dataset
            ? this.context.metadata.dataset
                  .filter((path) => this.validatePath(path, row.lines))
                  .map((path) => pathToOption(path))
            : [];
        const options = row.pathOptions.map((path) => pathToOption(path));
        return [].concat(defaultOptions, options);
    };

    createInfoYaml = () => {
        const rows = [...this.state.rows];
        const uniquePaths = new Set([].concat(...rows.map((row) => row.paths)));
        const uniquePathList = [...uniquePaths].sort();
        let info = {
            defaults: {
                application: "DaVinci/" + this.context.metadata.tupleTools.applicationInfo["DaVinci"],
                wg: "DPA", // TODO: get input from (LHCb) user. keep "DPA" if open data
                automatically_configure: true,
                inform: this.state.contactEmail,
                output: "DVNtuple.root", // Probably okay to leave this as the default
            },
        };
        rows.forEach((row) => {
            row.paths.forEach((path, _index) => {
                const jobID = uniquePathList.findIndex((otherPath) => otherPath === path);
                const key = `job${jobID}`;
                const dttFile = `${row.dtt.getSafeName()}.py`;
                if (key in info) {
                    info[key].options.push(dttFile);
                } else {
                    info[key] = {
                        input: {
                            bk_query: path,
                        },
                        options: [dttFile], // TODO: figure out the actual syntax that AnaProd needs to indicate that this is to be parsed into a DecayTreeTuple
                    };
                    if (path.includes("MDST")) {
                        const stream = row.lines[0].stream; // XXX: in the validation we enforce that each BK location be compatible with every line, so effectively forcing one stream per row
                        info[key].root_in_tes = `/Event/${stream}`;
                    }
                }
            });
        });
        return info;
    };

    uploadConfig = (e, id) => {
        let rows = [...this.state.rows];
        const index = this.state.rows.findIndex((item) => item.id === id);
        e.preventDefault();
        const reader = new FileReader();
        reader.onload = async (e) => {
            const text = e.target.result;
            const config = yaml.load(text);
            if (config.descriptorTemplate === rows[index].decay.descriptors.template) {
                rows[index].dtt = new DTTConfig(config, []);
                this.setInputs(id, rows[index].lines);
                this.setState({rows, showUploadModal: false});
                this.props.parentCallbackRows(rows);
            } else {
                alert("Make sure to upload a previously downloaded configuration file for the same decay.");
            }
        };
        reader.readAsText(e.target.files[0]);
    };

    handleShowUploadModal = (id) => {
        this.setState({
            showUploadModal: true,
            modalID: id,
        });
    };

    handleCloseUploadModal = () => {
        this.setState({
            showUploadModal: false,
        });
    };

    createPyOpts = (row) => {
        const subdirectory = this.state.productionName;
        const filename = row.dtt.getSafeName();
        return Array(...config.pyOptsTemplate)
            .join("\n")
            .replace("{subdirectory}", subdirectory)
            .replace("{filename}", filename);
    };

    processSubmission = () => {
        if (this.state.submitLocation) {
            this.setState({showReasonForRequestModal: true});
        } else {
            this.downloadAll();
        }
    };

    downloadAll = () => {
        const allFiles = this.generateAllFiles();

        return downloadZip(allFiles, `${this.state.productionName}.zip`);
    };

    generateAllFiles = () => {
        return [
            ...this.state.rows.map((row) => [`${row.dtt.getSafeName()}.yaml`, yaml.dump(row.dtt.config)]),
            ...this.state.rows.map((row) => [`${row.dtt.getSafeName()}.py`, this.createPyOpts(row)]),
            ["info.yaml", yaml.dump(this.createInfoYaml())],
        ];
    };

    submitRequest = (url) => {
        this.setState({
            requestLoading: true,
            showReasonForRequestModal: false,
            requestError: "",
            requestSubmitted: false,
        });

        const allFiles = this.generateAllFiles();

        const formData = new FormData();

        analysisHelpers.forEach((file) => {
            formData.append("analysisHelpersFiles[]", new Blob([file.content], {type: "text/plain"}), file.name);
        });

        allFiles.forEach((file) => {
            formData.append("generatedFiles[]", new Blob([file[1]], {type: "text/plain"}), file[0]);
        });

        formData.append("email", localStorage.getItem("email") || "");
        formData.append("name", this.state.productionName);
        formData.append("reasonForRequest", this.state.reasonForRequest);

        fetch(url, {
            method: "POST",
            body: formData,
        })
            .then((response) => {
                if (!response.ok) {
                    throw new Error(`HTTP error! Status: ${response.status}`);
                }
                this.setState({requestLoading: false, requestSubmitted: true});
            })
            .catch((error) => {
                console.error("Fetch error:", error);

                this.setState({
                    requestLoading: false,
                    requestError:
                        "Something went wrong while submitting your request. Please try again later or contact support.",
                });
            });
    };

    handleShowReasonForRequestModal = () => {
        this.setState({
            showReasonForRequestModal: true,
        });
    };

    handleCloseReasonForRequestModal = () => {
        this.setState({
            showReasonForRequestModal: false,
        });
    };

    dismissSubmitSuccess = () => {
        this.setState({requestSubmitted: false});
    };

    dismissSubmitError = () => {
        this.setState({requestError: ""});
    };

    render() {
        const emptySession =
            !this.state.productionName && this.state.contactEmail.length === 0 && this.state.rows.length === 0;
        const validEmail =
            this.state.contactEmail.length > 0 &&
            this.state.contactEmail.every((email) => EmailValidator.validate(email));
        const nRows = this.state.rows.filter((row) => row.dtt).length;
        const ready = nRows > 0 && nRows === this.state.rows.length;
        const readyPaths = this.state.rows.every((row) => row.paths.length > 0);

        // add a check for each row to see if it has an empty name or if it has a name that is not unique
        const hasValidNames = this.state.rows.every((row) => {
            const safeName = row.dtt ? row.dtt.getSafeName() : "";
            const allSafeNames = this.state.rows
                .filter((otherRow) => otherRow.dtt)
                .map((otherRow) => otherRow.dtt.getSafeName());

            return safeName.trim().length > 0 && allSafeNames.filter((name) => name === safeName).length === 1;
        });

        const tableContent = this.state.rows.map((row) => {
            const safeName = row.dtt ? row.dtt.getSafeName() : "";
            const allSafeNames = this.state.rows
                .filter((otherRow) => otherRow.dtt)
                .map((otherRow) => otherRow.dtt.getSafeName());
            const hasValidName =
                safeName.trim().length > 0 && allSafeNames.filter((name) => name === safeName).length === 1;
            const urls = row.lines.map((strline) => (
                <StrippingLine
                    key={`${strline.line}`}
                    line={strline.line}
                    stream={strline.stream}
                    versions={strline.versions}
                    showlink={true}
                />
            ));

            return (
                <Row key={row.id} className="align-items-center" style={{marginBottom: 10}}>
                    <Col lg>
                        <Card>
                            <Card.Header className="d-flex justify-content-between align-items-start">
                                {row.dtt ? (
                                    <InputGroup hasValidation>
                                        <FormControl
                                            placeholder="DecayTree"
                                            value={row.dtt.getName()}
                                            onChange={this.setRowName(row.id)}
                                            isInvalid={!hasValidName}
                                        />
                                        <FormControl.Feedback type="invalid">
                                            {row.dtt.getName().trim() === ""
                                                ? "Name cannot be empty"
                                                : "Must give a unique name"}
                                        </FormControl.Feedback>
                                    </InputGroup>
                                ) : (
                                    ""
                                )}
                                <ButtonGroup>
                                    {row.dtt ? (
                                        <>
                                            <Route
                                                render={({history}) => (
                                                    <OverlayTrigger
                                                        overlay={<Tooltip>{"Configure this DecayTreeTuple"}</Tooltip>}
                                                    >
                                                        <Button
                                                            type="submit"
                                                            variant={"secondary"}
                                                            onClick={() => {
                                                                this.handleSubmitSpecificRow(history, row.id);
                                                            }}
                                                            disabled={!hasValidName && !this.context.loaded.tupleTools}
                                                        >
                                                            <PencilSquare />
                                                        </Button>
                                                    </OverlayTrigger>
                                                )}
                                            />
                                            {this.state.hideDownloadButtons ? (
                                                ""
                                            ) : (
                                                <>
                                                    <OverlayTrigger
                                                        overlay={
                                                            <Tooltip>
                                                                Download DecayTreeTuple YAML configuration file
                                                            </Tooltip>
                                                        }
                                                    >
                                                        <Button
                                                            className="ms-auto"
                                                            type="button"
                                                            onClick={() => {
                                                                download(
                                                                    yaml.dump(row.dtt.config),
                                                                    `${row.dtt.getSafeName()}.yaml`
                                                                );
                                                            }}
                                                            disabled={!hasValidName}
                                                        >
                                                            <Download />
                                                        </Button>
                                                    </OverlayTrigger>
                                                    <OverlayTrigger
                                                        overlay={
                                                            <Tooltip>
                                                                Download python options file to load this DecayTreeTuple
                                                                from YAML
                                                            </Tooltip>
                                                        }
                                                    >
                                                        <Button
                                                            className="ms-auto"
                                                            type="button"
                                                            onClick={() =>
                                                                download(
                                                                    this.createPyOpts(row),
                                                                    `${row.dtt.getSafeName()}.py`
                                                                )
                                                            }
                                                            disabled={!this.state.productionName || !hasValidName}
                                                        >
                                                            <FileEarmarkCode />
                                                        </Button>
                                                    </OverlayTrigger>
                                                </>
                                            )}

                                            <DeleteButton action={this.handleRemoveSpecificRowDTT(row.id)} />
                                        </>
                                    ) : (
                                        <>
                                            <OverlayTrigger overlay={<Tooltip>{"Add a DecayTreeTuple"}</Tooltip>}>
                                                <Button
                                                    type="submit"
                                                    variant={"success"}
                                                    onClick={this.handleCreateDTT(row.id)}
                                                    disabled={!this.context.loaded.tupleTools}
                                                >
                                                    <PlusLg />
                                                </Button>
                                            </OverlayTrigger>
                                            <OverlayTrigger
                                                overlay={<Tooltip>Upload DecayTreeTuple configuration file</Tooltip>}
                                            >
                                                <Button
                                                    className="ms-auto"
                                                    type="button"
                                                    onClick={() => {
                                                        this.handleShowUploadModal(row.id);
                                                    }}
                                                >
                                                    <Upload />
                                                </Button>
                                            </OverlayTrigger>
                                            <Modal show={this.state.showUploadModal} animation={false}>
                                                <Modal.Header>
                                                    <Modal.Title> Upload configuration file </Modal.Title>
                                                </Modal.Header>
                                                <Modal.Body>
                                                    <Form.Group controlId="formFile" className="mb-3">
                                                        <Form.Control
                                                            type="file"
                                                            onChange={(event) => {
                                                                this.uploadConfig(event, this.state.modalID);
                                                            }}
                                                            onClick={(event) => {
                                                                event.target.value = null;
                                                            }}
                                                            disabled={!this.context.loaded.tupleTools}
                                                        />
                                                    </Form.Group>
                                                </Modal.Body>
                                                <Modal.Footer>
                                                    <Button variant="secondary" onClick={this.handleCloseUploadModal}>
                                                        Close
                                                    </Button>
                                                </Modal.Footer>
                                            </Modal>
                                        </>
                                    )}
                                </ButtonGroup>
                            </Card.Header>
                            <Card.Body>
                                <Decay decay={row.decay} display={false} />
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col lg>
                        <Select
                            isMulti
                            options={streamLinesToOption(row)}
                            defaultValue={row.lines.map((lineInfo) =>
                                Object({
                                    value: lineInfo.line,
                                    label: (
                                        <StrippingLine
                                            line={lineInfo.line}
                                            stream={lineInfo.stream}
                                            versions={lineInfo.versions}
                                            showlink={false}
                                        />
                                    ),
                                    group: lineInfo.stream,
                                })
                            )}
                            placeholder="Stripping line"
                            onChange={this.changeSelStrLines(row.id)}
                        />
                    </Col>
                    <Col xs="auto">
                        <OverlayTrigger
                            trigger="click"
                            placement="right"
                            rootClose
                            overlay={
                                <Popover>
                                    <Popover.Body>{urls}</Popover.Body>
                                </Popover>
                            }
                        >
                            <Button className="ms-auto" type="button" disabled={urls.length === 0}>
                                <QuestionCircle />
                            </Button>
                        </OverlayTrigger>
                    </Col>
                    <Col>
                        {/*TODO: New dataset selection box (see #8)>*/}
                        <Creatable
                            isMulti
                            options={this.defaultBKPaths(row.id) /*TODO: use ODP records when ready*/}
                            value={this.getBKPaths(row.id)}
                            isDisabled={row.lines.length === 0}
                            placeholder="Bookkeeping path"
                            onChange={this.pathChoiceHandler(row.id)}
                            onCreateOption={this.pathCreateHandler(row.id)}
                            closeMenuOnSelect={false}
                        />
                    </Col>
                    <Col xs="auto">
                        <DeleteButton action={this.handleRemoveSpecificRow(row.id)} />
                    </Col>
                </Row>
            );
        });
        return (
            <>
                {tableContent}

                {this.state.requestLoading ? <Spinner animation="border" role="status" className="mt-1 mb-2" /> : ""}
                {this.state.requestSubmitted ? (
                    <Row className="mt-3">
                        <Col>
                            <Alert variant="success" dismissible onClose={this.dismissSubmitSuccess}>
                                <Alert.Heading>Request submitted!</Alert.Heading>
                                {this.state.requestSubmittedMessage}
                            </Alert>
                        </Col>
                    </Row>
                ) : (
                    ""
                )}
                {this.state.requestError && (
                    <Row className="mt-3">
                        <Col>
                            <Alert variant="danger" dismissible onClose={this.dismissSubmitError}>
                                <Alert.Heading>An error occurred!</Alert.Heading>
                                {this.state.requestError}
                            </Alert>
                        </Col>
                    </Row>
                )}

                <Row>
                    <Col xs="auto">
                        <ButtonGroup>
                            <Route
                                render={({history}) => (
                                    <OverlayTrigger overlay={<Tooltip>Select decays</Tooltip>}>
                                        <Button
                                            type="submit"
                                            variant="success"
                                            onClick={() => this.handleAddRow(history)}
                                        >
                                            <PlusLg />
                                        </Button>
                                    </OverlayTrigger>
                                )}
                            />
                            <Route
                                render={({history}) => (
                                    <OverlayTrigger overlay={<Tooltip>Configure all DecayTreeTuples</Tooltip>}>
                                        <Button
                                            type="submit"
                                            variant="secondary"
                                            onClick={() => this.handleSubmitRows(history)}
                                            disabled={!ready}
                                        >
                                            <PencilSquare />{" "}
                                            <Badge pill bg="primary">
                                                {nRows}
                                            </Badge>
                                        </Button>
                                    </OverlayTrigger>
                                )}
                            />
                            {this.state.hideDownloadButtons ? (
                                ""
                            ) : (
                                <>
                                    <OverlayTrigger
                                        overlay={
                                            <Tooltip>
                                                Download Analysis Production configuration file (info.yaml)
                                            </Tooltip>
                                        }
                                    >
                                        <Button
                                            type="download"
                                            onClick={() => download(yaml.dump(this.createInfoYaml()), "info.yaml")}
                                            disabled={!ready || !readyPaths || !validEmail}
                                        >
                                            <Download />
                                        </Button>
                                    </OverlayTrigger>
                                </>
                            )}
                        </ButtonGroup>
                    </Col>
                </Row>

                <Row>
                    <Col xs={4}>
                        <InputGroup hasValidation>
                            <OverlayTrigger
                                overlay={
                                    <Tooltip>
                                        Name of this production. This will become the subdirectory in
                                        AnalysisProductions
                                    </Tooltip>
                                }
                            >
                                <InputGroup.Text>Production name</InputGroup.Text>
                            </OverlayTrigger>
                            <FormControl
                                value={this.state.productionName}
                                onChange={this.setProductionName}
                                isValid={this.state.productionName}
                                placeholder="MyAnalysis"
                            />
                        </InputGroup>
                        {this.state.emailIsKnown ? (
                            ""
                        ) : (
                            <InputGroup hasValidation>
                                <OverlayTrigger
                                    overlay={<Tooltip>Email addresses to notify (comma-separated)</Tooltip>}
                                >
                                    <InputGroup.Text>Email</InputGroup.Text>
                                </OverlayTrigger>
                                <FormControl
                                    type="email"
                                    value={this.state.contactEmail.join(",")}
                                    onChange={this.setContactEmail}
                                    isValid={validEmail}
                                    placeholder="name@example.com"
                                />
                            </InputGroup>
                        )}

                        <ButtonGroup>
                            {this.state.hideDownloadButtons ? (
                                ""
                            ) : (
                                <>
                                    <OverlayTrigger
                                        overlay={
                                            <Tooltip>Download the configuration files for this production</Tooltip>
                                        }
                                    >
                                        <Button
                                            disabled={
                                                !ready ||
                                                !readyPaths ||
                                                !this.state.productionName ||
                                                !(this.state.emailIsKnown ? true : validEmail) ||
                                                !hasValidNames
                                            }
                                            onClick={this.downloadAll}
                                            type="info"
                                        >
                                            <Download /> Download
                                        </Button>
                                    </OverlayTrigger>
                                </>
                            )}

                            {this.state.submitLocation ? (
                                <>
                                    <OverlayTrigger
                                        overlay={<Tooltip>Submit a request with the chosen configuration</Tooltip>}
                                    >
                                        <Button
                                            disabled={
                                                !ready ||
                                                !readyPaths ||
                                                !this.state.productionName ||
                                                !(this.state.emailIsKnown ? true : validEmail) ||
                                                !hasValidNames
                                            }
                                            onClick={this.handleShowReasonForRequestModal}
                                        >
                                            <Send /> Submit
                                        </Button>
                                    </OverlayTrigger>

                                    <Modal
                                        show={this.state.showReasonForRequestModal}
                                        onHide={this.handleCloseReasonForRequestModal}
                                        size="lg"
                                    >
                                        <Modal.Header closeButton>
                                            <Modal.Title>Reason for request</Modal.Title>
                                        </Modal.Header>
                                        <Modal.Body>
                                            <Alert>{this.state.requestReasonMessage}</Alert>

                                            <InputGroup hasValidation>
                                                <OverlayTrigger
                                                    overlay={
                                                        <Tooltip>Reason for requesting the chosen ntuples</Tooltip>
                                                    }
                                                >
                                                    <InputGroup.Text>Reason for request</InputGroup.Text>
                                                </OverlayTrigger>
                                                <FormControl
                                                    as="textarea"
                                                    rows={3}
                                                    value={this.state.reasonForRequest}
                                                    onChange={this.setReasonForRequest}
                                                    isValid={this.state.reasonForRequest}
                                                    placeholder="I need these ntuples for..."
                                                />
                                            </InputGroup>
                                        </Modal.Body>
                                        <Modal.Footer>
                                            <Button
                                                variant="primary"
                                                onClick={() => this.submitRequest(this.state.submitLocation)}
                                                disabled={
                                                    !ready ||
                                                    !readyPaths ||
                                                    !this.state.productionName ||
                                                    !this.state.reasonForRequest ||
                                                    !(this.state.emailIsKnown ? true : validEmail) ||
                                                    !hasValidNames
                                                }
                                            >
                                                Submit request
                                            </Button>
                                        </Modal.Footer>
                                    </Modal>
                                </>
                            ) : (
                                ""
                            )}

                            <DeleteButton action={this.clearAll} disabled={emptySession}>
                                Clear
                            </DeleteButton>
                        </ButtonGroup>
                    </Col>
                </Row>
            </>
        );
    }
}

LinesTable.propTypes = {
    rows: PropTypes.arrayOf(PropTypes.object),
    name: PropTypes.string,
    email: PropTypes.arrayOf(PropTypes.string),
    reasonForRequest: PropTypes.string,
    parentCallbackInfo: PropTypes.func,
    parentCallbackRows: PropTypes.func,
    decaysPath: PropTypes.string.isRequired,
    submitLocation: PropTypes.string,
    hideDownloadButtons: PropTypes.bool,
    emailIsKnown: PropTypes.bool,
    requestReasonMessage: PropTypes.string,
    requestSubmittedMessage: PropTypes.object,
};

export default LinesTable;
