/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import React from "react";
import PropTypes from "prop-types";
import Tooltip from "react-bootstrap/Tooltip";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";

const {search} = window.location;
const query = new URLSearchParams(search).get("s");
//const [searchQuery, setSearchQuery] = useState(query || '')

function compare(a, b) {
    return Object.keys(a)[0] > Object.keys(b)[0] ? 1 : Object.keys(a)[0] < Object.keys(b)[0] ? -1 : 0;
}

class ItemSearch extends React.Component {
    state = {
        items: this.props.data,
        label: this.props.label,
        searchQuery: query || "",
    };

    filterItems(variables, query) {
        /*
        const nInclOptions = variables.filter(
            variable => !Object.keys(variable)[0].toLowerCase().includes(query.toLowerCase())
        );
        */
        const inclOptions = variables.filter(
            (variable) =>
                Object.keys(variable)[0].toLowerCase().includes(query.toLowerCase()) &&
                Object.keys(variable)[0].toLowerCase() !== query.toLowerCase()
        );
        const matchedOptions = variables.filter(
            (variable) => Object.keys(variable)[0].toLowerCase() === query.toLowerCase()
        );

        const orderedNewOptions = matchedOptions.sort(compare);
        //this.setState({
        return orderedNewOptions.concat(inclOptions);
        //});
        /* if (!query) {
            return variables;
        }

        return variables.filter((variable) => {
            const name = Object.keys(variable)[0].toLowerCase();
            return name.includes(query.toLowerCase());
        });*/
    }

    render() {
        //const {decay} = this.props
        const filteredItems = this.filterItems(this.state.items, this.state.searchQuery);
        return (
            <>
                <form action="/" method="get">
                    <label htmlFor="header-search">
                        <span className="visually-hidden">Search item</span>
                    </label>
                    <input
                        value={this.state.searchQuery}
                        onInput={(e) => this.setState({searchQuery: e.target.value})}
                        type="text"
                        id="header-search"
                        placeholder={this.state.label}
                        name="s"
                    />
                </form>
                <div className="scroll">
                    <ul>
                        {filteredItems.map((variable, index) => (
                            <OverlayTrigger
                                placement="top"
                                delay={{hide: 450, show: 300}}
                                overlay={(props) => <Tooltip {...props}>{Object.values(variable)[0]}</Tooltip>}
                                key={index}
                            >
                                <p>{Object.keys(variable)[0]}</p>
                            </OverlayTrigger>
                        ))}
                    </ul>
                </div>
            </>
        );
    }
}

ItemSearch.propTypes = {
    data: PropTypes.arrayOf(PropTypes.object),
    label: PropTypes.string,
};
export default ItemSearch;

/* <div>
                    <ul>
                    {filteredVariables.map(variable => (
                            <li key={Object.keys(variable)[0]}>{Object.keys(variable)[0]}</li>
                        ))}
                    </ul>
                </div> */
