/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import React from "react";
import PropTypes from "prop-types";
import ConfigValue from "./ConfigValue";
import {Button, CloseButton, ListGroup, InputGroup} from "react-bootstrap";
import {PlusLg} from "react-bootstrap-icons";

/*
 * [int]      -> <ConfigList /> multiple rows of <input type="number" step=1 />
 * [uint]     -> <ConfigList /> multiple rows of <input type="number" step=1 min=0 />
 * [float]    -> <ConfigList /> multiple rows of <input type="number" />
 */

class ConfigList extends React.Component {
    handleUpdate = (index) => (value) => {
        let listValue = this.props.value;
        listValue[index] = value;
        this.props.callback(listValue);
    };
    handleAdd = () => {
        let listValue = this.props.value;
        const index = listValue.length;
        if (index < this.props.defaultValue.length) {
            const defaultValue = this.props.defaultValue[index].valueOf(); // Shallow copy
            listValue.push(defaultValue);
        } else {
            listValue.push(0);
        }
        this.props.callback(listValue);
    };
    handleRemove = (index) => () => {
        let listValue = this.props.value;
        listValue.splice(index, 1);
        this.props.callback(listValue);
    };
    render() {
        const configurables = this.props.value.map((value, index) => {
            const configProps = {
                type: this.props.type,
                value: this.props.value[index],
                callback: this.handleUpdate(index),
            };
            if (index < this.props.defaultValue.length) {
                configProps["defaultValue"] = this.props.defaultValue[index];
            }
            return (
                <ListGroup.Item key={index}>
                    <InputGroup>
                        <ConfigValue {...configProps} />
                        <CloseButton onClick={this.handleRemove(index)} />
                    </InputGroup>
                </ListGroup.Item>
            );
        });
        return (
            <>
                <ListGroup variant="flush">{configurables}</ListGroup>
                <Button variant="success" size="sm" onClick={this.handleAdd}>
                    <PlusLg />
                </Button>
            </>
        );
    }
}

ConfigList.propTypes = {
    type: PropTypes.string,
    callback: PropTypes.func,
    value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.bool,
        PropTypes.number,
        PropTypes.arrayOf(PropTypes.string),
    ]),
    defaultValue: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.bool,
        PropTypes.number,
        PropTypes.arrayOf(PropTypes.string),
    ]),
};
export default ConfigList;
