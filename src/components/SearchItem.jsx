/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import React from "react";
import PropTypes from "prop-types";
import Select from "react-select";
import Tooltip from "react-bootstrap/Tooltip";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";

function compare(a, b) {
    return a.value > b.value ? 1 : b.value > a.value ? -1 : 0;
}

class SearchItem extends React.Component {
    state = {
        variables: this.variablesOptions(this.props.variables),
    };
    variablesOptions(variables) {
        let result_options = [];
        //const options = list_to_options(variables)
        Object.keys(variables).forEach((variable) => {
            let value = variable;
            let label = (
                <OverlayTrigger
                    placement="left"
                    delay={{hide: 450, show: 300}}
                    overlay={(props) => <Tooltip {...props}>{variables[variable]}</Tooltip>}
                >
                    <p>{variable}</p>
                </OverlayTrigger>
            );
            result_options.push({value: value, label: label});
        });
        return result_options;
    }
    onInputChange = (inputValue) => {
        const nInclOptions = this.state.variables.filter(
            (variable) => !variable.value.toLowerCase().includes(inputValue.toLowerCase())
        );
        const inclOptions = this.state.variables.filter(
            (variable) =>
                variable.value.toLowerCase().includes(inputValue.toLowerCase()) &&
                variable.value.toLowerCase() !== inputValue.toLowerCase()
        );
        const matchedOptions = this.state.variables.filter(
            (variable) => variable.value.toLowerCase() === inputValue.toLowerCase()
        );

        const orderedNewOptions = matchedOptions.sort(compare);
        this.setState({
            variables: orderedNewOptions.concat(inclOptions).concat(nInclOptions),
        });
    };
    customSelectStyles = {
        option: (provided, state) => ({
            ...provided,
            borderBottom: "3px dotted red",
            color: state.isSelected ? "red" : "blue",
            padding: 10,
            width: 535,
        }),
        menu: (styles) => ({...styles, width: "540px", height: "35px"}),
    };

    render() {
        return (
            <div width="20px">
                <Select
                    isMulti
                    options={this.state.variables.sort((a, b) => a.value - b.value)}
                    styles={this.customSelectStyles}
                    onInputChange={this.onInputChange}
                />
            </div>
        );
    }
}

SearchItem.propTypes = {
    variables: PropTypes.arrayOf(PropTypes.object),
};
export default SearchItem;
