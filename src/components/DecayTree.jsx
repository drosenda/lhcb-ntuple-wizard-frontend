/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import React from "react";
import PropTypes from "prop-types";
import Graph from "react-graph-vis";
import ParticleTag from "./ParticleTag";
import DecayTag from "./DecayTag";
import Decay from "./Decay";
import ParticleConfig from "./ConfigNode";
import MetadataContext from "../contexts/MetadataContext";
import {
    Accordion,
    Button,
    ButtonGroup,
    Card,
    ListGroup,
    Stack,
    OverlayTrigger,
    Tooltip,
    Popover,
} from "react-bootstrap";
import {QuestionCircle, Lock, Unlock, Search, PencilSquare} from "react-bootstrap-icons";
import {MathComponent} from "mathjax-react";
import tex2svg from "../lib/mathjax";
import config from "../config.json";
import DTTConfig from "../lib/DTTConfig";
import _ from "lodash";

function htmlTitle(text) {
    const container = document.createElement("span");
    container.innerText = text;
    return container;
}

function makeSVG(text, colour = "black") {
    const pipes = "\\phantom{{}^{|-}_{|-}}"; // looks weird but helps ensure uniform text scaling
    const texSVG = tex2svg(`${pipes}${text}${pipes}`).innerHTML.replaceAll("currentColor", colour);
    return "data:image/svg+xml;charset=utf-8," + encodeURIComponent(texSVG);
}

function buildDecayGraph(decay, metadata) {
    const getGraphParams = (term, graph, parentId) => {
        if (!(term instanceof Array)) {
            const {branch, particle} = term;
            return {
                newNode: {
                    id: branch,
                    label: branch,
                    title: htmlTitle(metadata[particle].html),
                    image: {
                        selected: makeSVG(metadata[particle].latex, "#0d6efd"),
                        unselected: makeSVG(metadata[particle].latex, "black"),
                    },
                },
                newEdge:
                    parentId === 0
                        ? {}
                        : {
                              from: parentId,
                              to: branch,
                          },
            };
        }
        term.forEach((element) => {
            if (element instanceof Array) {
                const subParentId = graph.nodes.slice(-1)[0].id;
                graph = getGraphParams(element, graph, subParentId);
            } else {
                const {newNode, newEdge} = getGraphParams(element, graph, parentId);
                graph.nodes.push(newNode);
                graph.edges.push(newEdge);
            }
        });
        return graph;
    };

    let graph = getGraphParams(decay, {nodes: [], edges: []}, 0);
    graph.edges = graph.edges.filter((edge) => Object.keys(edge).length !== 0);

    return graph;
}

class DecayTree extends React.Component {
    static contextType = MetadataContext;
    static options = {
        ...config.dttGraphOptions,
        nodes: {
            // need to add a lambda to set the label hover-style
            ...config.dttGraphOptions.nodes,
            chosen: {
                label: (values, id, selected, _hovering) => {
                    if (selected) {
                        values.mod = "bold";
                        values.color = "#0d6efd";
                    }
                },
            },
        },
    };
    state = {
        graph: buildDecayGraph(this.props.decay.descriptors.mapped_list, this.context.metadata.particleProperties),
        dtt: new DTTConfig(_.cloneDeep(this.props.dttConfig), this.context.metadata.tupleTools.tupleTools),
        lastSave: _.cloneDeep(this.props.dttConfig),
        selectedParticleId: [],
        updated: false,
        lockGraphZoom: !config.dttGraphOptions.interaction.zoomView,
        showNodeModal: false,
    };

    network = null;

    selectAllByTag = (tag) => () => {
        const allBranches = this.state.dtt.config.branches;
        const matchingParticles = Object.keys(allBranches)
            .filter((branch) => {
                const particle = allBranches[branch].particle;
                const allTags = this.context.metadata.particleProperties[particle].tags;
                return allTags.includes(tag);
            })
            .sort();
        this.setState({selectedParticleId: matchingParticles});
        this.network.selectNodes(matchingParticles);
    };

    selectGroup = (group) => () => {
        const selection = group.split(",");
        this.setState({selectedParticleId: selection});
        this.network.selectNodes(selection);
    };

    countGroupTools = (group) => {
        const toolList = this.state.dtt.listTools(group);
        const nTools = toolList.length;
        return (
            <OverlayTrigger
                overlay={
                    <Popover>
                        <ListGroup>
                            {toolList.map((tool) => (
                                <ListGroup.Item key={tool}>{tool}</ListGroup.Item>
                            ))}
                        </ListGroup>
                    </Popover>
                }
            >
                <div>
                    {nTools} TupleTool{nTools === 1 ? "" : "s"}
                </div>
            </OverlayTrigger>
        );
    };

    callbackCloseNodeConf = () => {
        this.setState({
            showNodeModal: false,
        });
    };

    callbackSignalUpdated = () => {
        this.setState({
            updated: true,
        });
    };

    sendData = () => {
        const dttConfig = _.cloneDeep(this.state.dtt.config);
        this.setState({updated: false, lastSave: dttConfig});
        this.props.parentCallback(dttConfig);
    };

    revertDTT = () => {
        let dtt = this.state.dtt;
        dtt.config = _.cloneDeep(this.state.lastSave);
        this.setState({dtt: dtt, updated: false});
    };

    displaySelectionDescriptor = (decaySelector) => {
        // split into an array for easier looping
        // XXX: we could probably use descriptors.list in conjunction with descriptors.mapped_list here...
        const lokiArray = decaySelector.replace(/^\[/, "").replace(/^\^\[/, "^").replace(/\]CC$/, "").split(" ");
        const texArray = lokiArray.map((particle) => {
            if (particle === "->") {
                return "\\to";
            } else {
                let name = particle.replace(/^\^/, ""); // name will be used to look up the particle in metadata
                // detect and handle sub-decays (marked with round braces)
                // A -> (B -> C D) E
                // A -> (B -> C (D -> E F)) G
                let braces = ["", ""];
                const numBraces = [/\(/g, /\)/g].map((pattern) => (name.match(pattern) || []).length);
                if (name.startsWith("(")) {
                    name = name.replace(/^\(/, "");
                    braces[0] = "(";
                } else if (name.endsWith(")") && numBraces[1] > numBraces[0]) {
                    name = name.replace(/\)+$/, "");
                    braces[1] = ")".repeat(numBraces[1] - numBraces[0]);
                }
                // spit edge-cases to the console and handle them somewhat gracefully
                if (!Object.keys(this.context.metadata.particleProperties).includes(name)) {
                    console.error(`${name} not found in particle property metadata!`);
                    return `\\text{${particle}}`;
                }
                let latex = this.context.metadata.particleProperties[name].latex;
                // mark particles by underlining them
                // TODO: if we can figure out colour with mathjax-react, turn them blue instead
                if (particle.startsWith("^")) {
                    latex = `\\underline{${latex}}`;
                }
                // remember to put the braces back
                return `${braces[0]}${latex}${braces[1]}`;
            }
        });
        return texArray.join(" ");
    };

    displaySelection = (selection) => {
        const decaySelector = this.props.decay.descriptors.template.replace(/\${(\w+)}/g, (_, v) =>
            selection.includes(v) ? "^" : ""
        );
        const latex = this.displaySelectionDescriptor(decaySelector);
        return (
            <OverlayTrigger
                overlay={
                    <Popover>
                        <Popover.Header>LoKi selector</Popover.Header>
                        <Popover.Body>
                            <code>{decaySelector}</code>
                        </Popover.Body>
                    </Popover>
                }
            >
                <span>
                    <MathComponent tex={`${latex}`} display={false} />
                </span>
            </OverlayTrigger>
        );
    };

    toggleZoomLock = () => {
        this.network.setOptions({interaction: {zoomView: this.state.lockGraphZoom}});
        this.setState({lockGraphZoom: !this.state.lockGraphZoom});
    };

    render() {
        this.state.dtt.pruneGroups(); // Make sure no empty groups
        const selection = this.state.selectedParticleId;
        const tagInfo = this.context.metadata.userHints.particleTags;
        const particleInfo = this.context.metadata.particleProperties;
        const dtt = this.state.dtt.config;
        const nGroups = Object.keys(dtt.groups).length;
        const dttName = this.state.dtt.getSafeName();
        const decayTags = (
            <Stack direction="horizontal" style={{flexWrap: "wrap", justifyContent: "center"}} gap={2}>
                {this.props.decay.tags
                    .filter((tag) => this.context.metadata.userHints.decayTags[tag].warn)
                    .map((tag) => (
                        <DecayTag key={tag} tag={tag} />
                    ))}
            </Stack>
        );
        let selectedName = "";
        let filterToolTags = ["IParticleTupleTool"];
        let particleTags = [];
        let displayedGroups = {};
        if (selection.length > 0) {
            const target = this.state.dtt.getTarget(selection);
            const allParticles =
                selection.length > 1
                    ? target.particles.map((particle) => particleInfo[particle])
                    : [particleInfo[target.particle]];
            const allTags = new Set(
                [].concat(...allParticles.map((particle) => particle.tags.filter((tag) => !tagInfo[tag].hide)))
            );
            particleTags = [...allTags].filter((tag) => allParticles.every((particle) => particle.tags.includes(tag)));
            selectedName = this.displaySelection(selection);
            if (selection.length === 1) {
                const filteredGroups = Object.entries(dtt.groups).filter(([key, _value]) => key.includes(selection[0]));
                displayedGroups = Object.fromEntries(filteredGroups);
            }
        } else {
            filterToolTags.push("IEventTupleTool");
            const allBranches = dtt.branches;
            const allParticles = Object.keys(allBranches).map((branch) => particleInfo[allBranches[branch].particle]);
            const uniqueTags = new Set(
                [].concat(...allParticles.map((particle) => particle.tags.filter((tag) => !tagInfo[tag].hide)))
            );
            particleTags = [...uniqueTags];
            selectedName = <Decay decay={this.props.decay} display={false} />;
            displayedGroups = dtt.groups;
        }
        particleTags.sort((a, b) => tagInfo[a].group > tagInfo[b].group);
        const inputList =
            dtt.inputs.length === 0 ? (
                <ListGroup.Item>
                    Choose at least one Stripping line, otherwise no decay candidates will be selected
                </ListGroup.Item>
            ) : (
                dtt.inputs.map((location, index) => (
                    <ListGroup.Item key={index}>
                        <code>{location}</code>
                    </ListGroup.Item>
                ))
            );
        return (
            <Card>
                <Card.Header>
                    <OverlayTrigger
                        placement="bottom"
                        overlay={
                            <Popover>
                                <Popover.Body>
                                    The ROOT TTree will be found under <code>{`DecayTreeTuple/${dttName}`}</code>.
                                </Popover.Body>
                            </Popover>
                        }
                    >
                        <Card.Title>
                            Tree name: <code>{dttName}</code>
                        </Card.Title>
                    </OverlayTrigger>
                    <Accordion>
                        <Accordion.Item eventKey="0">
                            <Accordion.Header>
                                {dtt.inputs.length} input location{dtt.inputs.length === 1 ? "" : "s"}
                            </Accordion.Header>
                            <Accordion.Body>
                                <ListGroup variant="flush">{inputList}</ListGroup>
                            </Accordion.Body>
                        </Accordion.Item>
                    </Accordion>
                </Card.Header>
                <Card.Body>
                    <Card.Title className="d-flex justify-content-between align-items-start">
                        <span>
                            <OverlayTrigger
                                placement="bottom"
                                overlay={
                                    <Tooltip>
                                        Click to select particles to configure. Hold ctrl to select multiple. Clear the
                                        selection to configure the entire decay.
                                    </Tooltip>
                                }
                            >
                                <QuestionCircle />
                            </OverlayTrigger>{" "}
                            Configure <Decay decay={this.props.decay} display={false} />
                        </span>
                        <OverlayTrigger
                            placement="bottom"
                            overlay={
                                <Tooltip>
                                    Click to {this.state.lockGraphZoom ? "unlock" : "lock"} scroll-to-zoom.
                                </Tooltip>
                            }
                        >
                            <Button onClick={this.toggleZoomLock} variant="secondary-outline">
                                {this.state.lockGraphZoom ? <Lock /> : <Unlock />}
                                <Search />
                            </Button>
                        </OverlayTrigger>
                    </Card.Title>
                    {decayTags}
                    <Graph
                        graph={this.state.graph}
                        options={DecayTree.options}
                        events={{
                            selectNode: (event) => {
                                this.setState({selectedParticleId: [...event.nodes].sort()});
                            },
                            deselectNode: (event) => {
                                this.setState({selectedParticleId: [...event.nodes].sort()});
                            },
                        }}
                        getNetwork={(network) => {
                            this.network = network;
                        }}
                    />
                    <Card.Title>Select by category</Card.Title>
                    <Card.Body>
                        <Stack direction="horizontal" style={{flexWrap: "wrap", justifyContent: "center"}} gap={2}>
                            {particleTags.map((tag) => (
                                <ParticleTag as="button" key={tag} tag={tag} onClick={this.selectAllByTag(tag)} />
                            ))}
                        </Stack>
                    </Card.Body>
                    <Card.Title>Current selection: {selectedName}</Card.Title>
                    <ParticleConfig
                        dtt={this.state.dtt}
                        particleID={selection}
                        parentCallbackClose={this.callbackCloseNodeConf}
                        parentCallbackUpdated={this.callbackSignalUpdated}
                        filterToolTags={filterToolTags}
                        updated={this.state.updated}
                    />
                    {Object.keys(displayedGroups).length > 0 ? (
                        <Card>
                            <Card.Header>
                                {nGroups} group{nGroups === 1 ? "" : "s"}
                            </Card.Header>
                            <ListGroup variant="flush">
                                {Object.keys(displayedGroups).map((sel) => (
                                    <ListGroup.Item
                                        key={sel}
                                        className="d-flex justify-content-between align-items-start"
                                    >
                                        {this.displaySelection(sel.split(","))}
                                        {this.countGroupTools(sel.split(","))}
                                        <Button variant="outline-secondary" size="sm" onClick={this.selectGroup(sel)}>
                                            <PencilSquare />
                                        </Button>
                                    </ListGroup.Item>
                                ))}
                            </ListGroup>
                        </Card>
                    ) : (
                        ""
                    )}
                    <ButtonGroup>
                        <OverlayTrigger overlay={<Tooltip>Save changes to this DecayTreeTuple</Tooltip>}>
                            <Button type="submit" onClick={this.sendData} disabled={!this.state.updated}>
                                Save
                            </Button>
                        </OverlayTrigger>
                        <OverlayTrigger overlay={<Tooltip>Discard changes to this DecayTreeTuple</Tooltip>}>
                            <Button variant="outline-danger" onClick={this.revertDTT} disabled={!this.state.updated}>
                                Revert
                            </Button>
                        </OverlayTrigger>
                    </ButtonGroup>
                </Card.Body>
            </Card>
        );
    }
}

DecayTree.propTypes = {
    decay: PropTypes.object,
    dttConfig: PropTypes.object,
    parentCallback: PropTypes.func,
};
export default DecayTree;
