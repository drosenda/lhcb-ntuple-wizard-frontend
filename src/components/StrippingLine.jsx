/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import React from "react";
import PropTypes from "prop-types";
import {Stack} from "react-bootstrap";
import StrippingBadge from "./StrippingBadge";

class StrippingLine extends React.Component {
    render() {
        const strippingInfo = this.props.versions.map((version) => (
            <StrippingBadge
                key={`${this.props.line}-S${version}-badge`}
                version={version}
                showlink={this.props.showlink}
                {...this.props}
            />
        ));
        return (
            <span>
                {this.props.line}
                <br />
                <Stack direction="horizontal" style={{flexWrap: "wrap"}} gap={1}>
                    {strippingInfo}
                </Stack>
            </span>
        );
    }
}

StrippingLine.propTypes = {
    versions: PropTypes.arrayOf(PropTypes.string),
    line: PropTypes.string,
    showlink: PropTypes.bool,
};
export default StrippingLine;
