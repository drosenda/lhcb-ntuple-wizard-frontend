/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import React from "react";
import PropTypes from "prop-types";
import {OverlayTrigger, Tooltip, Spinner} from "react-bootstrap";
import MetadataContext from "../contexts/MetadataContext";
import {get_tupleTool_description} from "../lib/utils";

class TupleTool extends React.Component {
    static contextType = MetadataContext;

    render() {
        const toolClass = this.props.tool.split("/")[0];
        const description = this.context.loaded.tupleTools ? (
            get_tupleTool_description(toolClass, this.context.metadata.tupleTools.tupleTools)
        ) : (
            <Spinner animation="border" />
        );
        return (
            <OverlayTrigger overlay={<Tooltip>{description}</Tooltip>}>
                <span>{this.props.tool}</span>
            </OverlayTrigger>
        );
    }
}

TupleTool.propTypes = {
    tool: PropTypes.string,
};
export default TupleTool;
