/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import React from "react";
import PropTypes from "prop-types";
import {Badge, OverlayTrigger, Tooltip, Spinner} from "react-bootstrap";
import MetadataContext from "../contexts/MetadataContext";

class StrippingBadge extends React.Component {
    static contextType = MetadataContext;

    render() {
        // TODO: configurable baseURL, remove baseURL from metadata (or make it a top-level key)
        const url =
            this.props.line && this.context.loaded.stripping
                ? this.context.metadata.stripping[this.props.version][this.props.line].url[this.props.stream]
                : "http://lhcbdoc.web.cern.ch/lhcbdoc/stripping/config/stripping" + this.props.version + "/index.html";
        const {description} = this.context.loaded.strippingHints
            ? this.context.metadata.strippingHints[this.props.version]
            : {description: <Spinner animation="border" />};
        const badgeProps = this.props.showlink
            ? {
                  as: "a",
                  href: url,
                  target: "_blank",
              }
            : {};
        return (
            <OverlayTrigger overlay={<Tooltip>{description}</Tooltip>}>
                <Badge pill bg="info" {...badgeProps}>
                    {"S" + this.props.version}
                </Badge>
            </OverlayTrigger>
        );
    }
}

StrippingBadge.propTypes = {
    line: PropTypes.string,
    version: PropTypes.string,
    stream: PropTypes.string,
    link: PropTypes.string,
    showlink: PropTypes.bool,
};
export default StrippingBadge;
