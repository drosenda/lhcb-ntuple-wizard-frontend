/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
import PropTypes from "prop-types";
import {Badge, Alert, ListGroup, Spinner} from "react-bootstrap";
import {createContext, useContext, useState, useEffect} from "react";
import {loadDict} from "../lib/utils";
import config from "../config";

export const MetadataContext = createContext({});

export default MetadataContext;

export const useMetadataContext = (_init) => useContext(MetadataContext);

function MetadataProvider({children}) {
    let metadataTemplate = {};
    Object.keys(config.metadata_files).forEach((key, _index) => {
        metadataTemplate[key] = {};
    });
    const [metadata, setMetadata] = useState(metadataTemplate);

    let loadedTemplate = {};
    Object.keys(config.metadata_files).forEach((key, _index) => {
        loadedTemplate[key] = false;
    });
    const [loaded, setLoaded] = useState(loadedTemplate);

    const [error, setError] = useState(null);

    const getMetadata = async () => {
        Object.keys(config.metadata_files).forEach((key, _index) => {
            loadDict(config.metadata_files[key]).then(
                (result) => {
                    setMetadata((prevState) => ({
                        ...prevState,
                        [key]: result,
                    }));
                    setLoaded((prevState) => ({
                        ...prevState,
                        [key]: true,
                    }));
                },
                (e) => {
                    setError(e);
                }
            );
        });
    };

    useEffect(() => {
        getMetadata();
    }, []);

    if (error) {
        return <h1>Oops: {`${error}`}</h1>;
    } else {
        return (
            <MetadataContext.Provider value={{loaded: loaded, metadata: metadata}}>{children}</MetadataContext.Provider>
        );
    }
}

MetadataProvider.propTypes = {
    children: PropTypes.node,
};

export {MetadataProvider};

/* Wrap a component with <MetadataLoadWall> to show a loading screen until all metadata is fetched
 * This is the "lazy option". It's better UX to have a fine-grained response to individual keys in context.loaded
 * e.g. this.context.loaded.particleProperties? this.context.metadata.particleProperties[particle].html : particle;
 */
function MetadataLoadWall({children}) {
    const context = useMetadataContext();

    if (Object.values(context.loaded).some((v) => !v)) {
        const loadingSpinner = <Spinner animation="border" role="status" size="sm" />;
        const doneBadge = <Badge bg="success">Done</Badge>;
        const message = Object.keys(context.loaded).map((x) => (
            <ListGroup.Item key={x}>
                {x} {context.loaded[x] ? doneBadge : loadingSpinner}
            </ListGroup.Item>
        ));
        return (
            <>
                <Alert variant="warning">
                    Fetching metadata from the{" "}
                    <Alert.Link href={config.metadata_baseurl} target="_blank">
                        backend
                    </Alert.Link>
                    . This should take a few seconds.
                </Alert>
                <ListGroup variant="flush">{message}</ListGroup>
            </>
        );
    } else {
        return <>{children}</>;
    }
}

MetadataLoadWall.propTypes = {
    children: PropTypes.node,
};

export {MetadataLoadWall};
